const express = require('express');

const router = express.Router();

const Product = require('../models/Products')

const passport = require('passport');
const auth = require('./../middlewares/auth');

// File Handler - Multer - npm install multer
const multer = require('multer');
const storage = multer.diskStorage({
	destination: function(req,file,cb) {
		cb(null, 'public/products')
	},
	filename: function(req,file,cb) {
		cb(null, `${Date.now()}-${file.originalname}` );
	}
})

const upload = multer({storage : storage});



// Create
router.post('/', upload.single('image'), passport.authenticate('jwt', { session: false }), auth.checkIfAdmin, (req,res, next) => {
	// res.json(req.file);

	req.body.image = '/public/' + req.file.filename;
	Product.create(req.body)
	.then(product => {
		res.json(product)
	})
	.catch(next)
});

// Retrieve - Index
router.get('/', (req,res, next) => {
	Product.find()
	.then(products => {
		res.json(products)
	})
	.catch(next)
});

// Retrieve - Show
router.get('/:id', (req,res, next) => {
	Product.findById(req.params.id)
	.then(product => {
		res.json(product)
	})
	.catch(next)
});

// Update
router.put('/:id', passport.authenticate('jwt', { session: false }), auth.checkIfAdmin, (req,res, next) => {
	Product.findByIdAndUpdate(req.params.id,req.body,{new: true})
	.then(product => res.json(product))
	.catch(next)
});

// DESTROY
router.delete('/:id', passport.authenticate('jwt', { session: false }), auth.checkIfAdmin, (req,res, next) => {
	Product.findByIdAndDelete(req.params.id)
	.then(product => res.json(product))
	.catch(next)
});

module.exports = router;