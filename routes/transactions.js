const express = require('express');
const Product = require('./../models/Products')
const router =  express.Router();
const passport = require('passport');
const Transaction  = require('./../models/Transactions');
const auth = require('./../middlewares/auth');


router.post('/orders', passport.authenticate('jwt', { session: false }), auth.checkIfUser, (req,res,next) => {
	// res.send(req.body.orders);
	let orders = req.body.orders;

	let orderIds = [];

	orderIds =  orders.map( product => {
		return product.id;
	})

	Product.find({_id: orderIds})
	.then( products => {
		let total = 0;

		let newProducts = products.map( product => {
			let matchedProduct = {};
			orders.forEach( order => {
				if(product.id === order.id) {

					matchedProduct = {
						_id : product.id,
						name : product.name,
						price : product.price,
						image : product.image,
						quantity : order.qty,
						subtotal : order.qty * product.price
					}
				}
			})
			total += matchedProduct.subtotal;
			return matchedProduct;
		})
		res.send({
			products : newProducts,
			total
		});
	});
});

router.post('/', passport.authenticate('jwt', { session: false }), auth.checkIfUser, (req,res,next) => {

	let orders = req.body.orders;

	let orderIds = [];

	orderIds =  orders.map( product => {
		return product.id;
	})

	Product.find({_id: orderIds})
	.then( products => {
		let total = 0;

		let newProducts = products.map( product => {
			let matchedProduct = {};
			orders.forEach( order => {
				if(product.id === order.id) {

					matchedProduct = {
						productId : product.id,
						name : product.name,
						price : product.price,
						image : product.image,
						quantity : order.qty,
						subtotal : order.qty * product.price
					}
				}
			})
			total += matchedProduct.subtotal;
			return matchedProduct;
		})

		let transaction = {
			userId : req.user._id,
			transactionCode : Date.now(),
			products : newProducts,
			total
		}

		Transaction.create(transaction)
		.then(transaction => {return res.send(transaction)});

	});

})

router.put('/:id', passport.authenticate('jwt', { session: false }), auth.checkIfAdmin, (req,res,next) => {
	Transaction.findByIdAndUpdate(req.params.id, {status : req.body.status}, {new: true}) 
	.then( transaction => res.send(transaction));
})


module.exports = router;