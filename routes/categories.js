const express = require('express');
const passport = require('passport');
const auth = require('./../middlewares/auth');

// Require Router
const router = express.Router();

// Import Category Database Model
const Category = require('../models/Categories');

// Make Route
// Index
router.get('/', function(req, res, next) {
	// res.json({ message: 'This is index'})
	Category.find()
	.then(categories => {
		res.json(categories)
	})
	.catch(next);
});


// Show - /:id - wildcard
// req.params - get parameter values (addressbar)
router.get('/:id', (req,res, next) => {
	Category.findOne({_id : req.params.id})
	.then(category => res.json(category))
	.catch(next)
});


// CREATE
router.post('/', passport.authenticate('jwt', { session: false }), auth.checkIfAdmin, (req,res,next) => {
	// if(req.user.role !== 'admin') {
	// 	return res.send({message : "Unauthorized Request"});
	// }
	// console.log(req)

	// CREATE DATABAE ENTRY
	// Category.create({name:"Product"}, function(err, category) {
	// 	console.log(category);
	// 	res.send(category);
	// });

	Category.create(req.body)
	.then( (category)=> {
		res.send(category)
	}).catch(next)


});


// UPDATE
router.put('/:id', passport.authenticate('jwt', { session: false }), auth.checkIfAdmin, (req,res,next) => {
	// res.json({
	// 	data: "This is a put request",
	// 	id :req.params.id,
	// 	body: req.body
	// })

	Category.findOneAndUpdate(
		{
			_id : req.params.id
		},
		{
			name : req.body.name
		},
		{
			new : true,
			upsert: false
		}
	)
	.then(category => res.json(category))
	.catch(next)
});

router.delete('/:id', passport.authenticate('jwt', { session: false }), auth.checkIfAdmin, (req,res, next) => {
	// res.json ({
	// 		data : "This is for delete request",
	// 		id : req.params.id
	// 	})
	Category.findOneAndDelete({_id : req.params.id})
	.then(category => res.json(category))
	.catch(next)
});


// Export Module
module.exports = router;