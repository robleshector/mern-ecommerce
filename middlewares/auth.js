module.exports = {

checkIfAdmin :function (req,res,next) {
	if(req.user.role !== 'admin') {
		return res.send({message : "Unauthorized Request"});
	} else {
		next()
	}
},

checkIfUser : function (req,res,next) {
	if(req.user.role === 'admin') {
		return res.send({message : "Unauthorized Request"});
	} else {
		next()
	}
}

};