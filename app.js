const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const passport = require('passport');

// CORS -npm install cors
const cors = require('cors');


// ============= Set up Server =============

// Initialize App
const app = express();

// Set Port
const port = 3001;

// Connect to database
mongoose.connect(
	'mongodb://localhost:27017/myapp', 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	});

mongoose.connect('connected', ()=> {
	console.log('Database connected!');
})


// ============ MIDDLEWARES BEFORE ============
// 3rd party middleware
app.use(bodyParser.json());

// cors
app.use(cors());

// passport
app.use(passport.initialize());

// static
app.use('/public', express.static('public/products'));

// let myFunction = function(req,res,next) {
// 	console.log('May request!');
// 	next();
// }

// app.use(myFunction);


// ============ ROUTES ============

// ROUTER
app.use('/categories', require('./routes/categories'));
app.use('/products', require('./routes/products'));
app.use('/users', require('./routes/users'));
app.use('/transactions', require('./routes/transactions'));



// ============ MIDDLEWARES AFTER ============

// Error Handling Middleware
app.use(function(err,req,res,next){
	// console.log(err);
	res.status(400).json({
		error : err.message
	})
});



// ============ LISTEN TO PORT ============

// Start listening to port
app.listen(port, ()=> {
	console.log(`Listening to port ${port}`);
});