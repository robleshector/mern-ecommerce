const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TransactionSchema = new Schema ({
	userId : {
		type: String,
		required: true
	},
	createdAt : {
		type: Date,
		default: Date.now
	},
	transactionCode : {
		type: String,
		unique: true
	},
	status : {
		type: String,
		default: "Pending"
	},
	paymentMode : {
		type: String,
		default: "Over the counter"
	},
	products : [
		{
            productId: String,
            quantity: Number,
            subtotal: Number
        }
	],
	total: {
		type: Number,
		required: true
	}
});

const Transaction = mongoose.model('Transaction', TransactionSchema);
module.exports = Transaction;